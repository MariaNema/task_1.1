# Task 1.1
***The purpose*** of the repository is to get familiar with **git**.

Create a public repository hosted on GitLab (not GitHub).
Ensure that the repository has the following files to start with:
- readme.md
- .gitignore (matching your operating system)
- an image of a cat or dog (your preference)
- a text file called "old.txt" (Write anything inside it)
Do the following via DIFFERENT commits (each to their own):
- Add a different picture (perhaps of a tree or car?)
- Add a new markdown file ("picture.md"), its contents must describe the picture
- Delete "old.txt"
- Update "readme.md" to describe the purpose of the repository